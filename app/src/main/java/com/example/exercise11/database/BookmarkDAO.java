package com.example.exercise11.database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface BookmarkDAO {
    @Insert(onConflict = REPLACE)
    void insertBookmark(BookmarkEntity entity);

    @Update
    void editBookmark(BookmarkEntity entity);

    @Delete
    void deleteBookmark(BookmarkEntity entity);

    @Query("select * from bookmark")
    List<BookmarkEntity> getAllBookmark();

    @Query("select * from bookmark where id = :id")
    BookmarkEntity getBookmark(int id);

    @Query("delete from bookmark")
    void deleteAll();
}
